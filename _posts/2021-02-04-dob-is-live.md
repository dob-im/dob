---
layout: post
title: DOB live on Stellar Network
subtitle: Dobcoin launched on the Stellar Network
tags: [DOB, stellar, dobcoin, cryptocurrency]
comments: false
thumbnail-img: /assets/img/rocket.png
readtime: true
---

Dobcoin (DOB) is live on Stellar, see the full details [here](https://stellar.expert/explorer/public/asset/DOB-GA5QQMLTO6FM6ZRO4QGX266LSLNRO6THGH5WRJUITLOJVEVY35BVDOBC)

<iframe onLoad="var c=this;window.addEventListener('message',function({data,source}){if(c&&source===c.contentWindow&&data.widget===c.src)c.style.height=data.height+'px'},false);" src="https://stellar.expert/widget/public/asset/summary/DOB-GA5QQMLTO6FM6ZRO4QGX266LSLNRO6THGH5WRJUITLOJVEVY35BVDOBC" style="border:none;overflow:hidden;max-width:100%; min-width:300px;max-height:100%;min-height:200px;width:100%"></iframe>
