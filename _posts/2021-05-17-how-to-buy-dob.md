---
layout: post
title: How to buy and sell Dobcoin on LOBSTR 
tags: [DOB, XLM, buy dob, sell dob, exchange, wallet]
comments: false
readtime: true
---

# Step 1 - Create an Account to hold DOB
To get started we recommend creating an account by browsing to [https://lobstr.co](https://lobstr.co/authorization/signup/).
LOBSTR is one of the most popular and highly rated Stellar wallets, it is also available on [Android](https://play.google.com/store/apps/details?id=com.lobstr.client&hl=en_US&gl=US) and [iOS](https://apps.apple.com/us/app/lobstr-stellar-lumens-wallet/id1404357892)

<img src="https://dob.im/assets/img/guide11.png" />

Make sure to use a strong password when creating your LOBSTR account and when creating your new wallet it is very important to write down as securely store your recovery phrase. This will allow you to access your funds in the event of losing access to your account.

<img src="https://dob.im/assets/img/guide1.png" />

You can skip the “Set federation address” for now

<img src="https://dob.im/assets/img/guide2.png" />

# Step 2 - Purchase or transfer XLM
Once your account is created, you will either have to select “Receive” to transfer some XLM from another wallet/exchange or select “Buy Lumens” to purchase XLM with a credit card. LOBSTR uses Moonpay.io for credit card purchases which is a PCI compliant secure payment processor. 

## To Transfer XLM
Receive tokens by sending them from another wallet/exchange such as Coinbase, for example below we are sending 3 XLM from a Keybase wallet to our Lobstr wallet:

Copy receive tokens address:
<img src="https://dob.im/assets/img/guide3.png" width="600" />

Paste as to/destination in wallet/exchange: 
**Make sure the copied address matches the one shown as receive address in LOBSTR**
<img src="https://dob.im/assets/img/guide4.png" width="600" />

## To Purchase XLM
Select “Buy Lumens” then follow the instructions from Moonpay.io to purchase XLM. 

<img src="https://dob.im/assets/img/guide5.png" />

Once you have XLM in your wallet you can use it to purchase other assets on the Stellar blockchain. 

<img src="https://dob.im/assets/img/guide6.png" />

# Step 3 - Exchange XLM for DOB

In order to exchange DOB for XLM you have to add DOB to your Assets. This can be done by entering "dob" in the search box on the Assets page. Click “Add” to add DOB to your assets.

<img src="https://dob.im/assets/img/guide7.png"/>

On the Assets page you will see the option to exchange DOB for XLM, scroll down to make the exchange

<img src="https://dob.im/assets/img/guide8.png"/>

Enter the amount of DOB you would like to buy and select Buy DOB

<img src="https://dob.im/assets/img/guide9.png"/>

That’s it, you now own DOB congratulations! You can place future buy and sell orders for DOB on this page. 
