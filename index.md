---
layout: home
ext-css:
  - //fonts.googleapis.com/css?family=Roboto:400,700
ext-js:
  - //cdn.jsdelivr.net/npm/particles.js@2.0.0/particles.min.js
js:
  - /assets/js/particle.js
cover-img:
  - "/assets/img/bg4.jpg"
---

# About Dobcoin 

Dobcoin was created to bring integrity back to the cryptocurrency industry. With so many cryptocurrencies built on fleeting values, Dobcoin's best-in-breed strategy stands out from the pack. With integrity, loyalty, and valor, Dobcoin is ready to run with the big dogs. It is the cryptocurrency of choice for high-speed transactions.

For more information read our frequently asked questions below.

# Dobcoin FAQ 

## Why Dobcoin? 

- **Fast** - High speed and minimal cost transaction fees mean that it's a suitable replacement for existing payment methods. While Dogecoin, Bitcoin, ETH, and other popular cryptos measure transaction speed in minutes, Dobcoin transactions complete in mere seconds. 
- **Green** - Dobcoin is built on the energy-efficient, environmentally-friendly Stellar blockchain.
- **Easy to buy and sell** - You can easily buy and sell Dobcoin, no apps required or complicated pancake swap. It takes less than 5 minutes to set up an account at a decentralized exchange.
- **Decentralized and owned by you** - The makers of Dobcoin have released a majority of Dobcoin into circulation, so the true owners of Dobcoin are the holders of Dobcoin.
- **Transparent** - The blockchain is publicly accessible. All token details and all transactions can be viewed [here](https://stellar.expert/explorer/public/asset/DOB-GA5QQMLTO6FM6ZRO4QGX266LSLNRO6THGH5WRJUITLOJVEVY35BVDOBC).
- **Unpretentious** - A clear roadmap with realistic goals for making Dobcoin easy to buy, sell, and accept as payment for e-commerce and online stores.

### Technical Specs

You can't teach an old DOGE new tricks!

Dobcoin utilizes the fast and secure ed25519 signature scheme on the Stellar network. Transactions of Dobcoin on the Stellar network take approximately 2-5 seconds, or 100x faster than BTC which requires at least several minutes to reach consensus. When it comes to cryptocurrency adoption in the real world for products and services, transaction speed is key, and most of Dobcoin's competitors are dog-slow.

<img src="https://dob.im/assets/img/compare.jpg" alt="Compare DOBCOIN vs. Competitors" />

Dobcoin will only ever have 127000000000 tokens in existence and 75% were released on the open market. The initial market cap of Dobcoin is ~$175K USD. If Dobcoin even achieves 1% of the market cap of Dogecoin, early investors would see a 2000X increase in value, meaning a $100 USD investment would grow to $200,000 USD.

## Who owns dobcoin? 

As a decentralized token on the Stellar blockchain, Dobcoin is owned by its holders. Stellar has 4 key aspects when it comes to adding transactions to the ledger (or blockchain):

1) **Freedom of participation:** Anyone can join the consensus process, and no one has all or a majority of the decision-making power.

2) **Low latency:** Fast and cheap transactions are confirmed within a few seconds.

3) **Freedom of trust:** Nodes (those participating in the consensus system) choose their own set of trusted nodes and can revoke trust from bad actors at any time.

4) **Security:** Even if nodes fail or bad actors join the network, consensus can still be reached, and the network will come to the right conclusion.

# How to Buy Dobcoin

We recommend following our step-by-step instructions [here](https://dob.im/2021-05-17-how-to-buy-dob/) for getting started buying and selling Dobcoin.

**Exchanges Supporting Dobcoin**
- [Lobstr](https://lobstr.co/trade/DOB:GA5QQMLTO6FM6ZRO4QGX266LSLNRO6THGH5WRJUITLOJVEVY35BVDOBC)
- [Stellarport](https://stellarport.io/exchange/GA5QQMLTO6FM6ZRO4QGX266LSLNRO6THGH5WRJUITLOJVEVY35BVDOBC/DOB/native/XLM) - Hardware wallet support for Trezor & Ledger. 
- [StellarTerm](https://stellarterm.com/exchange/DOB-GA5QQMLTO6FM6ZRO4QGX266LSLNRO6THGH5WRJUITLOJVEVY35BVDOBC/XLM-native)
- [interstellar.exchange](https://interstellar.exchange/app/#/trade/guest/DOB/GA5QQMLTO6FM6ZRO4QGX266LSLNRO6THGH5WRJUITLOJVEVY35BVDOBC/XLM/native)

We are currently in contact with other exchanges and will update this list as new exchanges become available.

## Who is Sir Dobbinsworth? 

<p float="left">
  <FIGURE>
    <img src="https://dob.im/assets/img/dob-frame2.png" alt="Sir Dobbinsworth" width="500" />
    <FIGCAPTION>Meet Sir Dobbinsworth, Dobcoin's crypto mascot.</FIGCAPTION>
  </FIGURE>
</p>

By breed, he is a Jack Russell Terrier mix, small in stature but large in everything that matters. His loyalty and integrity are only surpassed by his valor. He has no use for memes. Rather than speaking in immature phrases like "such coin," he prefers to articulate himself as a professional. One cannot buy integrity, loyalty, or valor; however, for everything else, there is Dobcoin.

# Roadmap

Shopify, Magento, and WooCommerce are popular e-commerce platforms for online sellers. However, many sellers do not accept cryptocurrency payments as doing so can be complicated and difficult to implement. We intend to target these popular e-commerce platforms first and make accepting Dobcoin easy with simple, step-by-step implementation documentation and videos. There are no KYC or approvals required to accept Dobcoin as payment. The payments are peer-to-peer and you receive coins directly from the customer into any crypto wallet you want, with no intermediary.

## Accept Dobcoin as Payment on Shopify

If you have a Shopify store, you can accept Dobcoin as payment now by using the [Stellar Pay app](https://stellarpay.io/documentation/shopify/shopify-xlm-integration/). Our roadmap includes a plan to develop our Dobcoin payment solution that is as easy as installing a Shopify app. 

## Accept Dobcoin as Payment on Magento & WooCommerce
If you have a store that uses Magento or WooCommerce, you will be able to accept Dobcoin as payment. Our roadmap includes a plan to develop an easy to integrate Magento and WooCommerce extension.

## Create Dobpay Support Community 
Many store owners are hesitant to implement cryptocurrency payments due to lack of support. A community with support forum would be a place where store owners could collaborate and receive support for implementing Dobcoin and other direct cryptocurrency payment methods (Dobpay&trade;). 

## Growth

Our goal is to make Dobpay an easy to use and well supported direct (payer to receiver) cryptocurrency payment solution with no intermediary required. Here is our plan for the future of Dobcoin and Dobpay.

1) Dobpay will be free to use - Most other payment solutions charge at least a 5-10% transaction fee. 

2) Dobpay will always accept Dobcoin (DOB) and XLM - As adoption of Dobpay grows Dobcoin acceptance will also grow. 

3) Dobpay targeted marketing - Having a great payment solution is not always enough, we will use funds generated by sale of Dobcoin to target Dobpay ADs to communities where store owners go to seek answers on how to accept cryptocurrencies. For example, marketing the Dobpay shopify app on Reddit r/shopify. 

